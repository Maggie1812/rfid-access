from gpiozero import LED, Buzzer
from time import sleep
from datetime import datetime
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as Cipher_PKCS1_v1_5 
import time
import os
import subprocess
import json
import requests
import serial
import io


PATTERN_A = 0xAA
PATTERN_B = 0xBB
BAUDRATE = 9600
HEX_BASE = 16
URL = "https://t3ftcmdxqe.execute-api.us-east-1.amazonaws.com/fase1/registro"
URL_USERS = "https://t3ftcmdxqe.execute-api.us-east-1.amazonaws.com/fase1/consulta/usuarios"
POST_URL = "https://t3ftcmdxqe.execute-api.us-east-1.amazonaws.com/fase1/registro/log"
USERS_FILE = "/home/pi/Documents/IoT2019/users.txt"
RSA_PRIV = "/home/pi/Documents/IoT2019/id_rsa"
RSA_PUB = "/home/pi/Documents/IoT2019/id_rsa.pub"
RSA_EXT = "/home/pi/Documents/IoT2019/id_rsa.pub"
PARSING_SCRIPT = "/home/pi/Documents/IoT2019/uuid_parse"
RAW_DATA_FILE = "/home/pi/Documents/IoT2019/raw_data.txt"
HEX_INPUT_FILE = "/home/pi/Documents/IoT2019/hex_data.txt"
BKP_FILE = "/home/pi/Documents/IoT2019/transactions.bkp"
READ_PERMISSION = "r"
WRITE_PERMISSION = "w"
APPEND_PERMISSION = "a+"
CLEAR_LINE ='s|${line}|""|g' 
UPDATE_TIMEOUT = 2   #minutes
READ_TIMEOUT = 5   #seconds
DATE_FORMAT = "%d-%m-%Y"
HOUR_FORMAT = "%H:%M"

serialPort = serial.Serial()
json_data  = {}
uuids = set()
uuids_list = []
users = dict()
valid_users = set()
authorized_user = ""
privateKey = ""
myPublicKey = ""
extPublicKey = ""
now = datetime.now()
valid = False
networkConnection = True

def rsa_keys():
    global privateKey
    global myPublicKey
    global extPublicKey
    with open(RSA_PRIV, READ_PERMISSION) as privateKey_file:
        privateKey = RSA.importKey(privateKey_file.read())

    with open(RSA_PUB, READ_PERMISSION) as myPublicKey_file:
        myPublicKey = RSA.importKey(myPublicKey_file.read())
        
    with open(RSA_EXT, READ_PERMISSION) as extPublicKey_file:
        extPublicKey = RSA.importKey(extPublicKey_file.read())

    
def rsa_encrypt(raw_data, publicKey): 
    cipher = Cipher_PKCS1_v1_5.new(publicKey)
    return cipher.encrypt(raw_data.encode())


def rsa_decrypt(encrypted_data):
    global privateKey
    cipher = Cipher_PKCS1_v1_5.new(privateKey)
    return cipher.decrypt(encrypted_data, None).decode()
    
    
def post(json_body):
    global networkConnection
    global redLED
    try:
        response = requests.post(POST_URL, data=json_body)
        networkConnection = True
        redLED.off()
    except requests.exceptions.RequestException as err:
        print(err)
        networkConnection = False
        redLED.on()
    except requests.exceptions.RequestException as err:
        print(err)
        networkConnection = False
        redLED.on()
        
        
def reset():
    global uuids
    global uuids_list
    global users
    global valid_users
    global authorized_user
    global json_data
    global valid 
    #Remove all existing values
    uuids.clear()
    del uuids_list[:]
    users.clear()
    valid_users.clear()
    authorized_user = ""
    json_data = {}
    valid = False
    
    
def update_users():
    global networkConnection
    global redLED
    print("Updating database...")
    try:
        response = requests.get(url = URL_USERS)
        if response.status_code == 200:
            networkConnection = True
            redLED.off()
            json_body = response.content
            json_dict = json.loads(json_body)
            user_file = open(USERS_FILE, WRITE_PERMISSION)
            for item in json_dict["Items"]:
                uuid = (item['UUID'])['S']
                print(uuid)
                user_file.write(str(uuid) + '\n')
            user_file.close()
            sendBackup()
        else:
            print("Unable to update database. Running with last copy")
            networkConnection = False
            redLED.on()
    except requests.exceptions.ConnectionError:
        print("Network connection failed")
        networkConnection = False
        redLED.on()
    except requests.exceptions.RequestException:
        print("Something went wrong")
        networkConnection = False
        redLED.on()


    
        

def alarm():
    global buzzer
    global yellowLED
    buzzer.on()
    yellowLED.on()
    for i in range(1, 5):
        sleep(2)
        print("ALARM")
        yellowLED.toggle()
    buzzer.off()
    yellowLED.off()

    
def authorized_users():
    user_file = open(USERS_FILE, READ_PERMISSION)
    for line in user_file:
        user = line.strip('\n')
        valid_users.add(user)
    user_file.close()


def classify():
    global uuids
    global authorized_user
    for uuid in uuids:
        print(str(uuid))
        if uuid in valid_users:
            authorized_user = str(uuid)
            print("AUTHORIZED USER")
        else:
            uuids_list.append(uuid)
    if not authorized_user:
        print("NOT AUTHORIZED USER")


def backup(backup_data):
    print("Backing up")
    backup_file = open(BKP_FILE, APPEND_PERMISSION)
    encrypted = rsa_encrypt(backup_data, myPublicKey)
    backup_file.write(encrypted)
    backup_file.close()
    
    
def sendBackup():
    print("Send backup")
    global redLED
    backup_file = open(BKP_FILE, READ_PERMISSION)
    files = backup_file.readlines()
    print(files)
    backup_file.close()
    if len(files) > 0:
        for i in range(len(files)):
            currentFile = files.pop()
            status_code = put(rsa_decrypt(currentFile))
            if status_code != 200:
                networkConnection = False
                files.insert(currentFile)
                redLED.on()
                
        if len(files) > 0:
            backup_file = open(BKP_FILE, WRITE_PERMISSION)
            for file in files:
                backup_file.write(file + ';')
            backup_file.close()
        else:
            os.system("rm -f " + str(BKP_FILE))

        
    
def put(json_body):
    global URL
    global networkConnection
    try:
        response = requests.put(URL, data=json_body)
        return response.status_code
    except requests.exceptions.RequestException as err:
        print("Req exception")
        backup(json_body)
        return err
    except requests.exceptions.ConnectionError as err:
        print("Connection error")
        backup(json_body)
        return err
    
    


def json_transaction():
    print("JSON DICT") 
    global authorized_user
    global uuids_list
    global networkConnection
    global redLED
    date_string = now.strftime(DATE_FORMAT)
    time_string = now.strftime(HOUR_FORMAT)
    for uuid in uuids_list:
        global json_data 
        json_data = {
            "fecha" : date_string,
            "hora" : time_string,
            "responsableId" : authorized_user,
            "fileId" : uuid
            }
    print("JSON: ", json.dumps(json_data))
    json_body = json.dumps(json_data)
    status_code = put(json_body)
    if status_code == 200 and networkConnection == False:
        networkConnection = True
        sendBackup()
        redLED.off()
    elif status_code == 200 and networkConnection == True:
        redLED.off()
    else:
        networkConnection = False
        redLED.on()

def validate():
    global uuids_list
    if not authorized_user:
        alarm()
    elif len(uuids_list) == 0:
        date_string = now.strftime(DATE_FORMAT)
        time_string = now.strftime(HOUR_FORMAT)
        json_user = {
            "idUsuario" : authorized_user,
            "date" : date_string,
            "time" : time_string,
            }
        json_body = json.dumps(json_user)
        post(json_body)
    else:
        json_transaction()
    
def cast_data():
    hex_data = open(HEX_INPUT_FILE, READ_PERMISSION)
    for hex_uuid in hex_data:
        uuids.add(str(int(hex_uuid, 16)))
    hex_data.close()

    
def process():
    cast_data()
    authorized_users()
    classify()
    validate()

    
    
def init_actuators():
    global redLED
    global yellowLED
    global buzzer
    redLED = LED(14)
    yellowLED = LED(15)
    buzzer = Buzzer(18)


def init_port():
    serialPort.port = "/dev/ttyUSB0"
    serialPort.baudrate = BAUDRATE
    serialPort.bytesize = serial.EIGHTBITS
    serialPort.parity = serial.PARITY_NONE
    serialPort.stopbits = serial.STOPBITS_ONE
    
    try:
        serialPort.open()
    except:
        print("Could not open the serial Port")


def read_antenna():
    global now
    global valid
    reset()
    data_file = open(RAW_DATA_FILE, WRITE_PERMISSION)
    timeout = time.time() + READ_TIMEOUT
    while time.time() < timeout:
        data_size = serialPort.inWaiting()
        data = serialPort.read(data_size)
        if len(data) > 0:
            timeout = time.time() + READ_TIMEOUT
            valid = True
            now = datetime.now()
            repr_data = repr(data)
            data_file.write(repr_data)
    print("TIMEOUT")
    data_file.close()
    if valid:
        print("VALID DATA")
        subprocess.call(PARSING_SCRIPT)
        process()

def json_encrypt_test():
    global myPublicKey
    date_string = now.strftime(DATE_FORMAT)
    time_string = now.strftime(HOUR_FORMAT)

    json_data = {
            "fecha" : date_string,
            "hora" : time_string,
            "responsableId" : authorized_user,
            "fileId" : 123456
            }
    json_put = json.dumps(json_data)
    print(json_put)
    json_encrypted = rsa_encrypt(json_put, myPublicKey)
    json_decrypt = rsa_decrypt(json_encrypted)
    json_dict = json.loads(json_decrypt)
    print(json_dict)
    for item in json_dict:
        print(str(item) + ":" + str(json_dict[item]))


def main():
    if not os.path.isfile(BKP_FILE):
        os.system("touch " + BKP_FILE)
    init_actuators()
    init_port()
    update_users()
    last_updated = time.time()
    reset()
    rsa_keys()

    while True:
        if time.time() >=  last_updated + UPDATE_TIMEOUT*60:
            update_users()
            last_updated = time.time()
        read_antenna()



if __name__ == "__main__":
    main()